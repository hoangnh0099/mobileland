import 'package:flutter/material.dart';

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            padding: EdgeInsets.fromLTRB(20, 14, 20, 14),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.black12
            ),
            child: TextField(
              autofocus: false,
              keyboardType: TextInputType.text,
              decoration: InputDecoration.collapsed(
                hintText: "Search",
              ),
            ),
          ),
        )
      ],
    );
  }
}