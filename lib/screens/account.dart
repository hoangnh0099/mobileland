import 'package:flutter/material.dart';

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          onTap: () {},
          leading: Icon(Icons.account_circle),
          title: Text("Username"),
        ),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.add_shopping_cart),
          title: Text("Bought"),
        ),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.favorite),
          title: Text("Favorite"),
        ),
        ListTile(
          onTap: () {},
          leading: Icon(Icons.exit_to_app),
          title: Text("Sign out"),
        ),
      ],
    );
  }
}
