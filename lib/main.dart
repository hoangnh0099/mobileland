import 'package:flutter/material.dart';
import './screens/home.dart';
import './screens/search.dart';
import './screens/notify.dart';
import './screens/account.dart';

void main() => runApp(
  MaterialApp(
    title: "Mobileland",
    home: MyApp(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primaryColor: Colors.black,
      primaryColorDark: Colors.black,
    ),
  )
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Mobileland"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Cart())
                );
              },
            )
          ],
        ),
        bottomNavigationBar: Container(
          color: Colors.black,
          child: TabBar(
            indicatorColor: Colors.black,
            tabs: <Widget>[
              Tab(icon: Icon(Icons.home)),
              Tab(icon: Icon(Icons.search)),
              Tab(icon: Icon(Icons.notifications_none)),
              Tab(icon: Icon(Icons.supervisor_account)),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Home(),
            Search(),
            Notify(),
            Account(),
          ],
        ),
      )
    );
  }
}

class Cart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(child: Text("Cart Screen"));
  }
}