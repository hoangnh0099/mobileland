import 'package:flutter/material.dart';
import './../models.dart';

class Home extends StatelessWidget {
  final data = Data().data;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: <Widget>[
        ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return ListTile(title: Text(data[index]));
          },
        )
      ],
    );
  }
}
